# This is a sample YOLO algorithm script.

import cv2
import numpy as np
import matplotlib.pyplot as plt

# Read and assign deep learning network by using trained weights for object detection
yolo = cv2.dnn.readNet("yolov3.weights", config="yolov3.cfg")

# Create an empty list to import classes
classes = []

# Open class names file to split lines (each class)
with open ("coco.names", 'r') as f:
    classes = f.read().splitlines()

# Print the number of classes from pre-trained network
print("Number of classes : " + str(len(classes)))

# Load the image to be passed to network for object detection
img = cv2.imread("eagle.jpg")

# Save the image height and width to define bounding boxes accordingly
(image_height, image_width) = img.shape[:2]

# Print the image width and height
print("Image width & height : " + str(image_width) + " & " + str(image_height))

# Convert image format to blob format
blob = cv2.dnn.blobFromImage(img, 1/255, (320, 320), (0, 0, 0), swapRB=True, crop=False)

# Print image shape of blob format image
print("Blob image shape : " + str(blob.shape))

new_image = blob[0].reshape(320, 320, 3)

# Plot new image that have been obtained
plt.imshow(new_image)
plt.show()

# Set blob format image to yolo algorithm
yolo.setInput(blob)

output_layers_name = yolo.getUnconnectedOutLayersNames()
layer_output = yolo.forward(output_layers_name)

# Create an empty list to keep bounding boxes in the image
boxes = []
# Create an empty list to keep confidence values to predict objects
confidences = []
# Create an empty list to keep predicted class names
class_ids = []

for output in layer_output:
    for detection in output:
        score = detection[5:]
        class_id = np.argmax(score)
        confidence = score[class_id]
        if 0.7 < confidence:
            # box followed by the boxes' width and height
            box = detection[0:4] * np.array([image_width, image_height, image_width, image_height])
            (centerX, centerY, width, height) = box.astype("int")
            # use the center (x, y)-coordinates to derive the top and
            # and left corner of the bounding box
            x = int(centerX - (width / 2))
            y = int(centerY - (height / 2))
            # update list of bounding box coordinates, confidences,
            # and class IDs
            boxes.append([x, y, int(width), int(height)])
            confidences.append(float(confidence))
            class_ids.append(class_id)

print("Number of boxes : " + str(len(boxes)))

indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
font = cv2.FONT_HERSHEY_PLAIN
colors = np.random.uniform(0, 255, size=(len(boxes), 3))

if len(indexes) > 0:
    for i in indexes.flatten():
        x, y, w, h = boxes[i]
        label = str(classes[class_ids[i]])
        confi = str(round(confidences[i], 2))
        color = colors[i]
        cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
        cv2.putText(img, label + " " + confi, (x, y + 20), font, 1, (100, 255, 255), 2)

plt.imshow(img)
plt.show()




